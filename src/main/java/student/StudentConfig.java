package student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.util.List;

@Configuration
public class StudentConfig {
    @Bean
    CommandLineRunner commandLineRunner(StudentRepository repository) {
        return args -> {
            Student s1 = new Student(
                    "Franz",
                    "franz.reichel@htlstp.ac.at",
                    LocalDate.parse("1983-01-26")
            );

            Student s2 = new Student(
                    "Werner",
                    "werner.gitschthaler@htlstp.ac.at",
                    LocalDate.parse("1979-05-05")
            );

            repository.save(s1);
            repository.save(s2);
            repository.saveAll(List.of(s1,s2));
        };
    }
}
